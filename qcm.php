<?php include("head.php") ?>

<div id='bandeau-qcm'>
</div>

<div id="conteneur-general">

    <?php
    // Connexion à la base de données avec PDO
    include("connexionbdd.php");

    // Récupération de la clé du QCM (au format num_question;num_question...)
    $cle = base64_decode($_GET['cle']);
    $points_bonne_reponse = base64_decode($_GET['b']);
    $points_mauvaise_reponse = base64_decode($_GET['m']);


    // Création de la chaîne de caractère (num_question, num_question...) nécessaire
    // à la requête SQL
    $num_questions = explode(';', $cle);
    $ordres_rep = array();

    $tab_requete = "(";
    $rang = 0;
    foreach ($num_questions as $num) {
        $tab_requete = $tab_requete . $num . ",";
        $ordres_rep[$rang] = "ABCD";
        $rang++;
    }
    $tab_requete = substr($tab_requete, 0, -1) . ")";

    ?>

    <div style="display: none;" id="form-id">
        <h2>Merci de saisir votre <b>numéro</b> d'identification</h2>
        <br>
        <input type="number" name="nom" size="20" required>
        <br>
        <br>
        <button class='btn btn-primary' id="envoi-id">Commencer le QCM</button>
    </div>

    <a data-fancybox data-src="#form-id" data-modal="true" href="form-id" class="btn btn-primary" id='lien-fancy'></a>

    <div style="display: none;" id="changement-page">
        <h2>Vous avez quitté ou rechargé la page du QCM</h2>
        <h3>Veuillez attendre votre professeur</h3>
    </div>

    <a data-fancybox data-src="#changement-page" data-modal="true" href="changement-page" class="btn btn-primary" id='lien-changement'></a>

    <section class='qcm'>
        <h1 class='h1-qcm'>QCM de NSI</h1>
        <h4>
            <p>Une bonne réponse rapporte <?= $points_bonne_reponse ?> point(s). Une mauvaise retire <?= $points_mauvaise_reponse ?> point(s)</p>
            <p>Une absence de réponse n'est pas pénalisée</p>
            <p>Les points sont comptabilisés par domaine</p>
        </h4>
        <form method='POST' action='correction.php'>

            <input type="hidden" name="nom_eleve" size="30" value="" required>
            <input type="hidden" name="prenom_eleve" size="30" value="prenom" required>
            <input type="hidden" name="classe_eleve" size="10" value="classe" required>
            <input type="hidden" name="b" size="10" value="<?= base64_decode($_GET['b']) ?>">
            <input type="hidden" name="m" size="10" value="<?= base64_decode($_GET['m']) ?>">
            <?php
            // Récupération de toutes les domaines correspondants aux questions du GET
            $texte_req = 'SELECT questions.num_domaine FROM questions INNER JOIN domaines ON questions.num_domaine = domaines.num_domaine WHERE num_question IN ' . $tab_requete . '  GROUP BY domaines.num_domaine';
            $domaines = $bdd->prepare($texte_req);
            $domaines->execute();

            $domaines = $domaines->fetchAll(PDO::FETCH_ASSOC);

            $domaine_precedent = '';

            $numero_q = 1;

            $cle = '';
            $num_q = 0;

            foreach ($domaines as $domaine) :


                // Récupération de toutes les questions correspondants aux numéros du GET
                $texte_req = 'SELECT * FROM questions INNER JOIN domaines ON questions.num_domaine = domaines.num_domaine  WHERE num_question IN ' . $tab_requete . ' AND questions.num_domaine = ? ORDER BY num_question';
                $questions = $bdd->prepare($texte_req);
                $questions->execute(array($domaine['num_domaine']));

                $questions = $questions->fetchAll();

                shuffle($questions);

                foreach ($questions as $question) :
                    if ($question['domaine'] != $domaine_precedent) :
                        ?>
                        <div class='col-md-12'>
                            <h2 class='h2-domaine'><?= $question['domaine'] ?></h2>
                        </div>
            <?php

                        $domaine_precedent = $question['domaine'];
                    endif;

                    $cle .= $question['num_question'] . ';';
                    $ordres_rep[$num_q] = str_shuffle($ordres_rep[$num_q]);
                    $num_q++;

                    include("question.php");
                endforeach;
            endforeach;
            $cle = substr($cle, 0, -1);
            ?>

            <input type="hidden" value="<?= $_GET['cle'] ?>" name="cle" />
            <input type="hidden" value="<?= $_GET['p'] ?>" name="p" />
            <button class='btn btn-info' id='envoi-reponse'>Envoyer les réponses</button>

        </form>

    </section>

</div>

</body>

<script>
    $('document').ready(function() {

        $("#lien-fancy").fancybox().trigger('click');
        
        $('#envoi-id').click(function(event) {
            if ($('[name=nom]').val() == "") {
                alert("Vous devez saisir un identifiant");
            } else {
                $('[name=nom_eleve]').val($('[name=nom]').val());
                $('#bandeau-qcm').html("Votre identifiant : " + $('[name=nom]').val());
                $('#bandeau-qcm').css('display', 'block');
                $.fancybox.close();
            }
        })

        $('[name=nom]').keypress(function(e) {
            var keycode = (e.keyCode ? e.keyCode : e.which);
            if (keycode == '13') {
                if ($('[name=nom]').val() == "") {
                    alert("Vous devez saisir un identifiant");
                } else {
                    $('[name=nom_eleve]').val($('[name=nom]').val());
                    $('#bandeau-qcm').html("Votre identifiant : " + $('[name=nom]').val());
                    $('#bandeau-qcm').css('display', 'block');
                    $.fancybox.close();
                }
            }
        });

    })
</script>

</html>