<?php session_start();
include("head.php");

if (empty($_SESSION) or $_SESSION['connecte'] != true) :
    include("header.php");
    echo "Vous ne devriez pas être ici : <a href='index.php'>Retour</a>";
else :
    include('connexionbdd.php');
    include("header.php");
    include("nav.php");
    ?>
    <h1 class='h1-qcm'>Prise en main rapide de GeNumSI</h1>

    <h2>1) Générer son premier QCM</h2>
    <p>Pour générer votre QCM plusieurs options s'offrent à vous :</p>
    <img src="image_aide/creerQCM1.png" alt="Menu Créer un QCM">
    <ul>
        <li><strong>Niveau 1ère</strong> et <strong>Niveau Terminale</strong> vous permettent la génération automatique de QCM.
            Pour chacun de ces niveaux vous pouvez sélectionner le nombre de questions souhaitées par rubriques ou par sous-rubriques.
            Le générateur sélectionnera aléatoirement le nombre de questions souhaitées dans la base de questions.</li>
        <li> <strong>Sélection Générale</strong> permet de sélectionner une à une les questions que vous souhaitez parmi toutes les questions de la base.</li>
    </ul>
    <p>Pour chacune de ces options vous pouvez générer votre QCM dans différents formats:</p>
    <ul>
        <li>Une page Web permettant une évaluation/correction automatique du QCM.</li>
        <li>Un format HTML facilement convertible en PDF ou éditable avec OpenOffice.</li>
        <li>Un format HTML du corrigé du QCM.</li>
    </ul>
    <p>Une fois la génération terminée, un nouvel onglet s'ouvre dans votre navigateur contenant le QCM demandé.</p>
    <p>Vous remarquerez l'URL de votre QCM, qui a un format semblable à l'exemple ci-dessous :</p>
    <p><code>http://isnbreizh.fr/genumsi/qcm.php?cle=MTEzOzY0Ozk3&p=Mg%3D%3D&b=Mw==&m=MQ==</code></p>
    <p>Vous remarquerez les deux paramètres <strong>cle</strong> et <strong>p</strong>
        qui correspondent respectivement à la liste des numéros de questions dans la base
        et à l'identifiant de l'utilisateur (codés en base64) .</p>
    <p>Du coup si vous souhaitez évaluer vos élèves avec ce QCM il vous suffit de récupérer cette URL et de l'ajouter dans une page Web
        avec la balise <strong>&lt;a&gt;</strong> :</p>
    <pre><code>&lt;a target="_blank"   title="QCM" href="http://isnbreizh.fr/genumsi/qcm.php?cle=MTEzOzY0Ozk3&p=Mg%3D%3D&b=Mw==&m=MQ=="&gt;QCM test n°1&lt;/a&gt;</code></pre>
    <p>Voilà ce que cela donne intégré dans cette page d'aide : <a target="_blank" title="QCM" href="http://isnbreizh.fr/genumsi/qcm.php?cle=MTEzOzY0Ozk3&p=Mg%3D%3D&b=Mw==&m=MQ==">QCM test n°1</a></p>
    <h4>Quelques remarques :</h4>
    <ul>
        <li>Pour chaque génération les questions sont mélangées aléatoirement dans chacune des rubriques
            et pour chacune des questions les réponses sont également mélangées aléatoirement.
            Ainsi si 2 élèves lancent le QCM, ils auront les mêmes questions, mais dans un ordre différent et avec des réponses dans un ordre différent.</li>
        <li>De même, si vous générez la version à imprimer d'un QCM il vous suffit de réactualiser la page pour obtenir une version avec les questions et les réponses dans un ordre différent.</li>
        <li>Dans le cas de l'évaluation/correction automatique la notation est celle préconisée pour l'évaluation de fin de 1ère. A savoir : Une bonne réponse rapporte 3 points. Une mauvaise retire 1 point. Une absence de réponse n'est pas pénalisée.</li>
        <li>Les résultats des élèves sont consultables dans le menu :<strong>Résultats des élèves</strong>.</li>
    </ul>

    <h2>2) Ajout de vos questions</h2>
    <p>L'ajout de question se fait via le menu :</p>
    <img src="image_aide/operation_question1.png" alt="Menu Opération sur les questions">
    <p>Sélectionné <strong>Ajout</strong>, vous obtenez alors le formulaire de saisie suivant :</p>
    <img src="image_aide/ajout_question1.png" alt="Formulaire Ajout Question">
    <p>Comme vous pouvez le voir dans cet exemple vous pouvez saisir du code HTML. Voici quelques exemples :</p>
    <p>Pour le code Python vous pouvez utiliser la balise <strong>&lt;code&gt;</strong> pour un affichage "inline" :</p>
    <img src="image_aide/tag_code.png" alt="balise code"><br /><br>
    <p>Ainsi que la balise <strong>&lt;pre&gt;</strong> pour un affichage "block":</p>
    <img src="image_aide/tag_pre.png" alt="balise code"><br><br>
    <p>Pour les tables de vérités vous pouvez utiliser la classe <strong>table_verite</strong>: </p>
    <img src="image_aide/table_verite.png" alt="style table verite"><br><br>
    <p>Vous pouvez également adjoindre une image à votre question. Celle-ci ne doit pas excéder 300 Ko et être au format jpg, jpeg ou png.</p>
    <img src="image_aide/ajout_question2.png" alt="Formulaire Ajout Question"><br><br>
    <p>Reste à définir la bonne réponse, ainsi que la rubrique/sous-rubrique correspondant à votre question :</p>
    <img src="image_aide/ajout_question3.png" alt="Formulaire Ajout Question"><br><br>
    <p>Vous pouvez alors visualiser le rendu de votre question (en vert la bonne réponse).</p>
    <img src="image_aide/ajout_question4.png" alt="Formulaire Ajout Question">
    <p>Si le résultat vous convient il vous suffit alors de cliquer sur le bouton permettant d'insérer votre question dans la base :</p>
    <img src="image_aide/ajout_question5.png" alt="Formulaire Ajout Question">

<?php
endif;
?>

<?php include("footer.php") ?>

</body>

</html>