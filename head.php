<!DOCTYPE html>

<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>GeNumSi</title>
    <link rel="shortcut icon" type="image/x-icon" href="image/logo-genumsi.png" />
    <script src="js/jquery-3.4.1.min.js"></script>
    <script src="fancybox/jquery.fancybox.min.js"></script>
    <link href="fancybox/jquery.fancybox.min.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <script src="js/bootstrap.bundle.min.js"></script>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/styleAjout.css">
    <link rel="stylesheet" href="css/styleQuestion.css">
</head>

<body>