<?php session_start();
include("head.php");

if (empty($_SESSION) or $_SESSION['connecte'] != true) :
    include("header.php");
    echo "Vous ne devriez pas être ici : <a href='index.php'>Retour</a>";
else :
    include('connexionbdd.php');
    include("header.php");
    include("nav.php");
    ?>

    <h1 class='h1-qcm'>Quelques QCM clé en main</h1>

    <p>Si vous souhaité générer/lier l'un de ces QCM avec votre identifiant : Choisir le menu <strong>[Créer un QCM/ A partir d'une liste]</strong></p>
    <br>

    <ul>
        <li>
            <h3>QCM sujet zéro - juillet 2019</h3>
            <p>191;192;193;194;195;196;215;145;216;217;218;219;149;224;225;226;142;227;260;261;241;242;243;262;228;229;230;231;232;233;208;209;210;143;211;212;197;198;200;199;201;202</p>
        </li>
        <br>
        <li>
            <h3>QCM sujet zéro initial - mai 2019</h3>
            <p>220;94;221;222;28;32;223;234;235;263;264;265;8;10;30;205;206;207;203;204;214</p>
        </li>
    </ul>

<?php
endif;
?>

<?php include("footer.php") ?>

</body>

</html>