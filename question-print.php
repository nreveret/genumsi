<li>
    <b>Question n°<?= $numero_q ?>:</b>
    <?php $numero_q++; ?>
    <div class="texte-question">
        <?= $question['question'] ?>
    </div>
    <?php

    if (!is_null($question['image'])) :
        ?>

        <img class='img-question-print' src="image_questions/<?= $question['image'] ?>">

    <?php endif; ?>

    <?php for ($i = 0; $i < 4; $i++) : ?>
        <div>
            <span class='check-unicode'>&#9744;</span>
            <span class='reponse-print'><?= $question['reponse' . $ordres_rep[$numero_q - 2][$i]] ?></span>
        </div>

        <br>

    <?php endfor ?>

    <div>
        <span class='check-unicode'>&#9744;</span>
        <span class='reponse-print'>Je ne sais pas...</span>
    </div>


    <br>

</li>

<br>