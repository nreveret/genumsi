<?php session_start();

include("head.php");

if (empty($_SESSION) or $_SESSION['connecte'] != true) :
    include("header.php");
    echo "Vous ne devriez pas être ici : <a href='index.php'>Retour</a>";
else :

    include("connexionbdd.php");
    include("header.php");
    include("nav.php");

    include "phpqrcode/qrlib.php";

    include("url-qcm.php");


    $req_increment = $bdd->prepare('UPDATE informations_admin SET qcms = qcms + 1 WHERE 1');
    $req_increment->execute();

    // Tableau contenant le nombre questions de chaque domaine (indexé par le domaine)
    $tab_questions_sous_domaine = array();
    $i = 1;
    foreach ($_POST as $key => $quest_par_sous_domaine) {
        $tab_questions_sous_domaine[substr($key,  5)] = $quest_par_sous_domaine;
        $i++;
    }

    $cle = '';

    foreach ($tab_questions_sous_domaine as $key => $nb) {
        if ($nb > 0) {
            $req_quest = $bdd->prepare("SELECT * FROM questions WHERE num_sous_domaine = ?");
            $req_quest->execute(array($key));

            $tab = $req_quest->fetchAll();

            if ($nb == 1) {
                $cles_aleatoires = array(array_rand($tab, $nb));
            } else if ($nb > 1) {
                $cles_aleatoires = array_rand($tab, $nb);
            }

            foreach ($cles_aleatoires as $cle_aleatoire) {
                $cle .= $tab[$cle_aleatoire]['num_question'] . ';';
            }
        }
    }
    $cle = substr($cle, 0, -1);

    $num_questions = explode(';', $cle);

    $cle64 = base64_encode($cle);

    ?>

    <h1 class='h1-qcm'>Validation du QCM</h1>

    <p>Votre QCM comporte les questions numéros (dans la base données) :</p>

    <ul>

        <?php foreach ($num_questions as $n) : ?>
            <li><?= $n ?></li>

        <?php endforeach ?>

    </ul>

    <h2>Voici un aperçu de celui-ci. Afin d'alléger la page, les réponses ne sont pas affichées.</h2>

    <?php
        //=====================================QMC inséré
        // Création de la chaîne de caractère (num_question, num_question...) nécessaire
        // à la requête SQL
        $tab_requete = "(";
        foreach ($num_questions as $num) {
            $tab_requete = $tab_requete . $num . ",";
        }

        $tab_requete = substr($tab_requete, 0, -1) . ")";


        // Récupération de toutes les questions correspondants aux numéros du GET
        $texte_req = 'SELECT * FROM questions INNER JOIN domaines ON questions.num_domaine = domaines.num_domaine WHERE num_question IN ' . $tab_requete . ' ORDER BY domaines.num_domaine, num_question ';
        $questions = $bdd->prepare($texte_req);
        $questions->execute();

        $domaine_precedent = '';

        $numero_q = 0;

        ?>

    <section class='qcm'>
        <h1 class='h1-qcm'>QCM de NSI</h1>

        <?php
            // Génération du code html du QCM
            while ($question = $questions->fetch()) :
                if ($question['domaine'] != $domaine_precedent) :

                    ?>
                <h2 class='h2-domaine'><?= $question['domaine'] ?></h2>
            <?php
                        $domaine_precedent = $question['domaine'];
                    endif;
                    $numero_q++;
                    ?>
            <li>
                <b>Question n°<?= $numero_q ?> :</b>

                <?= $question['question'] ?>

                <?php
                        if (!is_null($question['image'])) :
                            ?>

                    <img class='img-question' src="image_questions/<?= $question['image'] ?>">

                <?php endif; ?>


                <div class='reponse-qcm-valide'>
                    <p class='reponses-affiche'>Réponses :</p>
                    <ul type='A' class='reponses-cachees'>
                        <li class='reponse'><?= $question['reponseA'] ?></li>
                        <li class='reponse'><?= $question['reponseB'] ?></li>
                        <li class='reponse'><?= $question['reponseC'] ?></li>
                        <li class='reponse'><?= $question['reponseD'] ?></li>
                    </ul>
                </div>
            </li>
            <br>
        <?php endwhile ?>

    </section>

    <?php ?>
    <div class="validation-qcm">
                    <p>Avant de le partager, vous devez décider des points à attribuer.</p>
                    <p>Pour rappel la notation se fait par domaine : pour chacun d'entre eux, une note est calculée
                        en tenant compte des points ajoutés ou soustraits (une absence de réponse n'est pas pénalisée)</p>
                    <p>La note attribuée à un domaine ne peut pas être négative</p>
                    <p>La note totale au QCM est égale à la somme des notes par domaines</p>
                    <ul>
                        <li>
                            <p>Pour une <b>bonne</b> réponse :</p>
                            <div class='input-group-points'>
                                <div class="number-input">
                                    <span class="plus-moins-span" id="span-moins-b-bas">-</span>
                                    <input class="custom-select-perso number-inp" min="0" value="3" step="0.5" type="number" id='b-bas'>
                                    <span class="plus-moins-span" id="span-plus-b-bas">+</span>
                                </div>
                            </div>
                        </li>
                        <li>
                            <p>Pour une <b>mauvaise</b> réponse (ces points seront soustraits) :</p>
                            <div class='input-group-points'>
                                <div class="number-input">
                                    <span class="plus-moins-span" id="span-moins-m-bas">-</span>
                                    <input class="custom-select-perso number-inp" min="0" value="1" step="0.5" type="number" id='m-bas'>
                                    <span class="plus-moins-span" id="span-plus-m-bas">+</span>
                                </div>
                            </div>
                        </li>
                    </ul>
                    <p>Si ce qcm vous plaît vous pouvez : </p>
                    <form type='get' action='qcm.php' class='form-valide' target="_blank" id="form-qcm-bas">
                        <div class='ul-valide'>
                            <input type="hidden" name='cle' value='<?= $cle64 ?>'>
                            <input type="hidden" name='p' value='<?= base64_encode($_SESSION['num_util']) ?>'>
                            <input type="hidden" name='b' value="<?= base64_encode("3") ?>" id="hidden-b-bas">
                            <input type="hidden" name='m' value="<?= base64_encode("1") ?>" id="hidden-m-bas">
                            <button class='btn btn-info btn-valide' type='button' id="btn-qcm">Accéder au QCM complet</button>
                        </div>
                    </form>

                    <form type='get' action='qcm-print.php' class='form-valide' target="_blank">
                        <input type="hidden" name='cle' value='<?= $cle64 ?>'>
                        <input type="hidden" name='b' value="<?= base64_encode("3") ?>" id="hidden-print-b-bas">
                        <input type="hidden" name='m' value="<?= base64_encode("1") ?>" id="hidden-print-m-bas">
                        <button class='btn btn-info btn-valide' type='submit'>Accéder au QCM complet à imprimer</button>
                    </form>

                    <form type='get' action='qcm-corrige.php' target="_blank" class='form-valide'>
                        <input type="hidden" name='cle' value='<?= $cle64 ?>'>
                        <button class='btn btn-info btn-valide' type='submit'>Accéder au corrigé complet</button>
                    </form>

                    <p class='p-lien-qcm'>Le partager avec vos élèves à l'aide de ce lien :
                        <input type='text' class='lien-qcm' id='lien-qcm-bas' value='<?= url() ?>?cle=<?= urlencode($cle64) ?>&p=<?= urlencode(base64_encode($_SESSION['num_util'])) ?>' onclick='this.select() '>
                    </p>

                    <p class='p-lien-qcm'>Générer un QR-code pour le partager :</p>
                    <form method="post" action="qcm-qrcode.php" id="form-qrcode-bas" target="_blank">
                        <input type="hidden" name='cle' value='<?= urlencode($cle64) ?>'>
                        <input type="hidden" name='p' value='<?= urlencode(base64_encode($_SESSION['num_util'])) ?>'>
                        <input type="hidden" name='b' val="" id="hidden-qrcode-b-bas">
                        <input type="hidden" name='m' val="" id="hidden-qrcode-m-bas">
                        <button class='btn btn-info btn-valide' type='button' id="btn-qrcode-bas">Générer le qrcode</button>
                    </form>
                </div>

<?php
endif;

include("footer.php")
?>

</body>
<script>
    $('document').ready(function() {

        let lien = "<?= url() ?>?cle=<?= urlencode($cle64) ?>&p=<?= urlencode(base64_encode($_SESSION['num_util'])) ?>";
        $('#lien-qcm-bas').val(lien + "&b=" + window.btoa("3") + "&m=" + window.btoa("1"));

        $('#btn-qrcode-bas').click(function() {
            $('#hidden-qrcode-b-bas').val((window.btoa("" + $('#b-bas').val())));
            $('#hidden-qrcode-m-bas').val((window.btoa("" + $('#m-bas').val())));
            $("#form-qrcode-bas").submit();
        })

        $('#btn-qcm').click(function() {
            $('#hidden-b-bas').val((window.btoa("" + $('#b-bas').val())));
            $('#hidden-m-bas').val((window.btoa("" + $('#m-bas').val())));
            $("#form-qcm-bas").submit();
        })

        //Boutons du bas
        $('#span-moins-b-bas').click(function() {
            this.parentNode.querySelector('input[type=number]').stepDown();
            let bon = window.btoa("" + $('#b-bas').val());
            let mauvais = window.btoa("" + $('#m-bas').val());
            $('#hidden-b-bas').val(bon);
            $('#hidden-print-b-bas').val(bon);
            $('.lien-qcm').val(lien + "&b=" + bon + "&m=" + mauvais)
        })

        $('#span-plus-b-bas').click(function() {
            this.parentNode.querySelector('input[type=number]').stepUp();
            let bon = window.btoa("" + $('#b-bas').val());
            let mauvais = window.btoa("" + $('#m-bas').val());
            $('#hidden-b-bas').val(bon);
            $('#hidden-print-b-bas').val(bon);
            $('.lien-qcm').val(lien + "&b=" + bon + "&m=" + mauvais)
        })

        $('#span-moins-m-bas').click(function() {
            this.parentNode.querySelector('input[type=number]').stepDown();
            let bon = window.btoa("" + $('#b-bas').val());
            let mauvais = window.btoa("" + $('#m-bas').val());
            $('#hidden-m-bas').val(mauvais);
            $('#hidden-print-m-bas').val(mauvais);
            $('.lien-qcm').val(lien + "&b=" + bon + "&m=" + mauvais)
        })

        $('#span-plus-m-bas').click(function() {
            this.parentNode.querySelector('input[type=number]').stepUp();
            let bon = window.btoa("" + $('#b-bas').val());
            let mauvais = window.btoa("" + $('#m-bas').val());
            $('#hidden-m-bas').val(mauvais);
            $('#hidden-print-m-bas').val(mauvais);
            $('.lien-qcm').val(lien + "&b=" + bon + "&m=" + mauvais)
        })

        $('#b-bas, #m-bas').change(function() {
            let bon = window.btoa("" + $('#b-bas').val());
            let mauvais = window.btoa("" + $('#m-bas').val());
            $('#hidden-m-bas').val(mauvais);
            $('#hidden-print-m-bas').val(mauvais);
            $('.lien-qcm').val(lien + "&b=" + bon + "&m=" + mauvais)
        })

    })
</script>

</html>